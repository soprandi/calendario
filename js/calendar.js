/*global $*/
/*global Collection*/
/*global monthShortName*/
/*global dayName*/

//var oggi = "09/06/2017";
var vediCome ={ settimana:0, settimane:1, mese:2 };
var color = [{"id":1, "color":"#80aaff"},
             {"id":2, "color":"#e6eeff"},
             {"id":3, "color":"#d9ffb3"},
             {"id":4, "color":"#66cc00"},
             {"id":5, "color":"#e6b3cc"},
             {"id":6, "color":"#e6ccb3"},
             {"id":7, "color":"#e6e600"},
             {"id":8, "color":"#c2c2d6"},
             {"id":9, "color":"#a3c2c2"}];  
                          

/* DATI */
var colonne =  [
    {"id": "0", "descrizione": "#","classe":""},
    {"id": "1", "descrizione": "Stato","classe":"stato"},
    {"id": "2", "descrizione": "Veicolo","classe":"veicolo"},
    {"id": "3", "descrizione": "Targa","classe":"targa"},
    {"id": "4", "descrizione": "<span class=\"glyphicon glyphicon-envelope\"></span>","classe":"autosostituva"},
    {"id": "5", "descrizione": "Auto","classe":"targa"}
     ];

var auto = [
    {"idauto":1, "descrizione":"FIAT PUNTO","targa":"PP99987","default":false},
    {"idauto":2, "descrizione":"FIAT PANDA","targa":"XX123FF","default":false},
    {"idauto":3, "descrizione":"DA DEFINIRE","targa":"","default":true }
    ];        
    
var pratiche =[
        {"idcolore":3,"idveicolo": 11, "autosostitiva":true, "idauto":2, "stato":"riparazione", "descrizione":"Mercedes Benz", "targa":"RR089YI","dataIN":"30/05/2017","oraIN":"08:00 AM","dataOUT":"02/06/2017","oraOUT":"11:00 AM"},
        {"idcolore":1,"idveicolo": 1, "autosostitiva":true, "idauto":1, "stato":"riparazione", "descrizione":"Megane Coupè", "targa":"EC010MM","dataIN":"06/06/2017","oraIN":"08:00 AM","dataOUT":"07/06/2017","oraOUT":"11:00 AM"},
        {"idcolore":9,"idveicolo": 2, "autosostitiva":false,"idauto":null, "stato":"prenotazione", "descrizione":"Fiat Punto", "targa":"PX909OP","dataIN":"10/06/2017","oraIN":"12:00 AM","dataOUT":"15/06/2017","oraOUT":"14:00 PM"},
        {"idcolore":5,"idveicolo": 3, "autosostitiva":true, "idauto":3,"stato":"riparazione", "descrizione":"BMW 380", "targa":"TT871OR","dataIN":"07/06/2017","oraIN":"15:00 PM","dataOUT":"10/06/2017","oraOUT":"09:00 AM"},
        {"idcolore":7,"idveicolo": 12, "autosostitiva":true, "idauto":null,"stato":"riparazione", "descrizione":"Dacia Duster", "targa":"EP264XW","dataIN":"29/06/2017","oraIN":"15:00 PM","dataOUT":"03/07/2017","oraOUT":"09:00 AM"}
    ];
    


var lavorazioni = [
    {"id":"20170530_11","descrizione":"verniciatura"},
    {"id":"20170531_11","descrizione":"verniciatura"},
    {"id":"20170606_1","descrizione":"verniciatura"},
    {"id":"20170601_11","descrizione":"verniciatura"},
    {"id":"20170607_1","descrizione":"consegna"},
    {"id":"20170610_2","descrizione":"stacco"},	
    {"id":"20170611_2","descrizione":"lattoneria"},	
    {"id":"20170612_2","descrizione":"lattoneria"},	
    {"id":"20170613_2","descrizione":"verniciatura"},	
    {"id":"20170614_2","descrizione":"meccanica"},	
    {"id":"20170615_2","descrizione":"consegna"},	
    {"id":"20170607_3","descrizione":"stacco"},	
    {"id":"20170602_11","descrizione":"consegna"},
    {"id":"20170608_3","descrizione":"lattoneria"},	
    {"id":"20170609_3","descrizione":"verniciatura"},	
    {"id":"20170610_3","descrizione":"consegna"},
    {"id":"20170629_12","descrizione":"stacco"},
    {"id":"20170630_12","descrizione":"lattoneria"},
    {"id":"20170701_12","descrizione":"lattoneria"},
    {"id":"20170702_12","descrizione":"verniciatura"},
    {"id":"20170703_12","descrizione":"consegna"},
    ];

/* CALENDARIO */
var _calendario = function(){
  
   var me = this;
   me.containerName = 'mainContent'; 
   me.container = $('#' + me.containerName);
   me.content = '';
   me.vediCome = vediCome.settimane; // vediCome.mese; //vediCome.settimane; // <-- visualizzazione standard
   me.spostamento = 14;
   
   /* ---------- */
   /* COLLECTION */
   /* ---------- */
   me.colonne = new Collection();
   me.giorni = new Collection();
   me.pratiche = new Collection();
   me.colori = new Collection();
        
   me.empty = function(){
        try {
             me.container.empty();
        } catch (e) { alert('empty ERROR: ' + e.message)};  
   };
   
   /* ---------------- */
   /* RENDER CONTENUTI */                        
   /* ---------------- */
   me.render = function (){
        try {
           me.empty();
           me.creaPeriodo();
           
           var pos , pos1 = '';
           
           var classe ='calendario';
           
            switch (me.vediCome){
             case vediCome.mese:
                 pos = '<center>';
                 pos1 = '</center>'
                 classe=  'calendarioMese';
                 break
            }
           
           me.content = pos +'<table class="' + classe + '" cellpadding="0" cellspacing="0">';
           
           /* Intestazioni colonne + intestazioni giorni */
           me.renderIntestazioni();

           /* Pratiche */    
            switch (me.vediCome){
                case vediCome.settimana:
                case vediCome.settimane:
                    me.rendeRighe();
                    break;
            }
           
           me.content += '</table>'+ pos1;
           me.container.html(me.content);
           switch (me.vediCome){
                case vediCome.mese:
                    me.rendeRighe();
                    break;
            }
           me.appearance();    
           me.applicaFiltri();
           
        } catch (e) { alert('render ERROR: ' + e.message)};    
   };
   
   me.applicaFiltri = function() {
       var statoSel =  $('#cmbStato').val();
       var cls  = '' 
       switch (calendario.vediCome ) {
           case vediCome.mese:
               cls = 'm_pratiche' ;
               break;
           default:
               cls = 'pratiche' ;
               break;
        }; 
        
        $('.' + cls).each(function(){
            if (statoSel == 'tutti' || $(this).data('stato') == statoSel){
                 $(this).show();
            } else {
                 $(this).hide();
            }
        });
   };
   
   me.renderIntestazioni = function(){
       switch (me.vediCome){
           case vediCome.settimana:
           case vediCome.settimane:
                me.content += '<tr>';  
                me.renderColonne();  
                me.renderGiorni();
                me.content += '</tr>';
                
                // Riga separatrice 
                me.content += '<tr class="separatrice"><td colspan="' + me.colonne.count + me.giorni.count + '"></td></tr>';
                break;
           case vediCome.mese:
                me.content += '<tr>' + 
                              '  <th class="dayname" title="lun">lun</th>' + 
                              '  <th class="dayname" title="mar">mar</th>' + 
                              '  <th class="dayname" title="mer">mer</th>' + 
                              '  <th class="dayname" title="gio">gio</th>' + 
                              '  <th class="dayname" title="ven">ven</th>' + 
                              '  <th class="dayname" title="sab">sab</th>' + 
                              '  <th class="dayname" title="dom">dom</th>' + 
                              '</tr>';
                me.renderGiorniMese();
                break;
       }
   }
   
   /* Visualizzazione Settimane */
   me.renderColonne = function(){
     me.colonne.forEach(function(c){
          c.render(); 
          me.content += c.html;
     }); 
   }
   
   me.renderGiorni = function(){
     var i = 0;
     me.giorni.forEach(function(g){
          g.render(i); 
          me.content += g.html;
          i++;
     }); 

   }
   
   me.renderGiorniMese = function(){
     var i = 0;
     me.giorni.forEach(function(g){
       if (i == 0){
          me.content +='<tr>';
       }
            
       g.render(i); 
       me.content += g.html;

       
       if (i > 0 && g.dayWeek == 0){
           me.content +='</tr><tr>';
       }   
       i++;
     }); 
     
     me.content +='</tr>';
   
   }
   
   /* Righe */
   me.rendeRighe = function(){
      me.pratiche.forEach(function(pratica){
         switch (me.vediCome){
             case vediCome.settimana:
             case vediCome.settimane:
                 pratica.render(); 
                 me.content += pratica.html.replace('</giorni>', me.renderGiorniPratica(pratica));
                 break;
             case vediCome.mese:
                 me.renderGiorniPratica(pratica);    
                 break;
         }
     }); 
   }
   
   me.righeAppearance = function(){
      $('.pratiche').hover(function(){
         $(this).find('.selector').css("background-color","#BB0000");
         $(this).find('.targa, .descrizione').css("font-weight","bold");
         }, function(){
         $(this).find('.selector').css("background-color", "white");
         $(this).find('.targa, .descrizione').css("font-weight","normal")
      }); 
   }
   
   me.renderGiorniPratica = function(pratica){
       var i = 0;
       var ev = false; 
       var h = '';
       
       var strtDt  = new Date(pratica.dataIn.substr(6,4) + '-' + pratica.dataIn.substr(3,2) + '-' + pratica.dataIn.substr(0,2));
       var endDt  = new Date(pratica.dataOut.substr(6,4) + '-' + pratica.dataOut.substr(3,2) + '-' + pratica.dataOut.substr(0,2));
       
       me.giorni.forEach(function(giorno){
           var style = '';
           var content = '';
           var classe = 'col_' + (i %2 == 1 ? '0' : '1');
           var id =  giorno.data.substr(6,4) + giorno.data.substr(3,2) + giorno.data.substr(0,2) + '_' + pratica.idVeicolo;
           
           if (giorno.nome.toLowerCase() == 'domenica'){
               classe = giorno.nome.toLowerCase();
           }
           
           if (giorno.data == oggi.dataEstesa()){
               classe = 'oggi';
           }

           if ( giorno.date  >= strtDt && giorno.date <= endDt){ ev = true;};
           
           if (giorno.data == pratica.dataIn){
               style = "border-top-left-radius: 15px; border-bottom-left-radius: 15px;";
               content = pratica.oraIn;
           }
           
           if (giorno.data == pratica.dataOut){
              style = "border-top-right-radius: 15px; border-bottom-right-radius: 15px;";  
              content = pratica.dataOut;
           }
           
           
           switch (me.vediCome){
              case vediCome.settimana:
              case vediCome.settimane:
                if (ev){
                    
                    h+= '<td id="' + id +'" class="' + classe + '"><div class="' + (pratica.stato == 'prenotazione' ? 'prenotazione' : ottieniLavorazione(id)) + '" style="' + style +'">' + content + '</div></td>';
                } else {
                    h+= '<td  class="' + classe + '"></td>';
                }
                break;
              case vediCome.mese:
                if (ev){
                    
                    $('#cont_' + giorno.data.substr(6,4) + giorno.data.substr(3,2) + giorno.data.substr(0,2)).after('<tr><td><div class="m_pratiche" data-stato="' + pratica.stato + '" style="background-color:' + pratica.colore() +'; border:none">' + pratica.targa + ':  ' + ottieniLavorazione(id) + '</div></td></tr>'); 
                }
                
                break;                  
            }      
           
           if (giorno.data == pratica.dataOut){
               ev = false;
           }
           i++;    
       });
       
       return h;
   }
   
   /* Funzioni Aspetto */
   me.appearance = function(){
      /* Comportamento su hover riga */ 
      me.righeAppearance();
   }
   
   /* Funzioni Periodo */
   me.creaPeriodo = function(){
     // pulisco la collection attuale 
     me.giorni = new Collection();

     switch(me.vediCome) {
         case vediCome.settimana:
             //alert('settimana');
             me.spostamento = 7;
             calcolaGiorniPeriodo(me.spostamento);
              break;
         case vediCome.settimane:
             //alert('settimane');
             me.spostamento = 14;
             calcolaGiorniPeriodo(me.spostamento)
             break;
         case vediCome.mese:
             //alert('mese');
             me.spostamento = null;
             calcolaGiorniPeriodo();
             break;
     }
   }

   ///me.setFilter = function(){
   ///    
   ///}
   
   /* -------------------- */
   /* POPOLO LE COLLECTION */                        
   /* -------------------- */
   /* creo le colonne */    
   if (colonne.length > 0){
       $.each(colonne, function (c) {
           try {
               me.colonne.add('td_' + colonne[c].id, new _colonna({ id: colonne[c].id, descrizione: colonne[c].descrizione, html: null, classe: colonne[c].classe }));
           } catch (e) { alert('colonne.add ERROR: ' + e.message)}
       });
   }

  /* creo le righe delle pratiche che visualizzerò */   
  if (pratiche.length > 0){
      $.each(pratiche, function (p) {
            try {
                 me.pratiche.add('td_v_' + pratiche[p].idveicolo, new _pratica({idVeicolo: pratiche[p].idveicolo, 
                                                                                autoSostitiva: pratiche[p].autosostitiva, 
                                                                                idAuto:pratiche[p].idauto, 
                                                                                stato: pratiche[p].stato,
                                                                                descrizione:pratiche[p].descrizione,
                                                                                targa: pratiche[p].targa,
                                                                                dataIn:pratiche[p].dataIN,
                                                                                oraIn:pratiche[p].oraIN,
                                                                                dataOut:pratiche[p].dataOUT,
                                                                                oraOut:pratiche[p].oraOUT,
                                                                                idColore:pratiche[p].idcolore}));
           } catch (e) { alert('pratiche.add ERROR: ' + e.message)}
      });
  }
  
  if (color.length > 0){
       $.each(color, function (c) {
           try {
              
               me.colori.add(color[c].id, new _colore({ id: color[c].id, colore: color[c].color}));
           } catch (e) { alert('colonne.add ERROR: ' + e.message)}
       });
   }

};

var _colore = function(dati){
    var me = this;
    me.id = null;
    me.colore = null;
    
     try {
        if (dati) {
            // Uso l'extend perchè il dati sarà definito come un oggetto di questo tipo
            $.extend(me, dati);
        }
    } catch (e) { alert('_colore.init ERROR: ' + e.message)}
}

/* colonne */
var _colonna = function(dati) {
    var me = this;
    me.id = null;
    me.descrizione = null;
    me.html = null;
    me.classe = null;
    
    me.render = function(){
        me.html = '<td data-id="' + me.id + '" class="' + me.classe + '">' + me.descrizione + '</td>';
    };
    
    try {
        if (dati) {
            // Uso l'extend perchè il dati sarà definito come un oggetto di questo tipo
            $.extend(me, dati);
        }
    } catch (e) { alert('_columns.init ERROR: ' + e.message)}
    
};

/* _giorni */
var _giorno = function(dati) {
    var me = this;
    me.data = null;
    me.nome = null;
    me.html = null;

    me.render = function(c){
        var contenuto = '';
        switch (calendario.vediCome) {
            case vediCome.settimana:
            case vediCome.settimane:
                 var classe = 'col_' + (c %2 == 1 ? '0' : '1');
                 if (me.nome.toLowerCase() == 'domenica'){
                     classe = me.nome.toLowerCase();
                 }
            
                 if (me.data == oggi.dataEstesa()){  
                     classe = 'oggi';
                 }
                 
                 contenuto = '<td class="giorno ' + classe +'"><div class="day_0">' + me.nome + '</div><div class="day_1">' + me.data + '<div></td>';
                break;
            
            case vediCome.mese:
                // Una riga è composta da 7 colonne dove la prima posizione è il lunedi che corrisponde al giorno 1 
                // se sto disegnato il primo giorno del mese devo anche creare la riga di intestazione
                if (c==0){
                    //alert(dayName[me.dayWeek] + ' ' +  me.date);
                    contenuto += ottieniStrutturaSettimana(me.dayWeek, me.date);
                }
                
                if (me.nome.toLowerCase() == 'domenica'){
                    classe = me.nome.toLowerCase() + 'Mese';
                }
                
                if (me.data == oggi.dataEstesa()){  
                    classe = 'oggiMese';
                }
                
                if (me.month != oggi.month){
                    classe = 'cellaMesePrec';
                }
                
                contenuto +=  '<td class="day '+ classe + '" id="day_' + me.id + '">' + 
                              ' <div class="cellaMese " title="' + me.data + '">' + 
                              '  <table>' +
                              '     <tr><td class="int">'  + parseInt(me.day) + (parseInt(me.day) == 1 ? ' ' + monthShortName[parseInt(me.month) -1].toLowerCase() + (parseInt(me.month) == 1 ? ' ' + me.year : '') :'') + '<td></tr>' +
                              '     <tr id="cont_' + me.id + '"><td class="cont"><td></tr>' +
                              '  </table>' +
                              ' </div>' + 
                              '</td>';
                                
                break;
        }
        
        me.html = contenuto;

    };
    
    me.dayInMonth = function(){
        return  me.date.getMonthDayCount();
    }

    try {
        if (dati) {
            $.extend(me, dati);
            
            /* Proprità aggiunte */
            me.date = new Date(me.data.substr(6,4) + '-' + me.data.substr(3,2) + '-' + me.data.substr(0,2));
            me.day=me.data.substr(0,2);
            me.month=me.data.substr(3,2);
            me.year=me.data.substr(6,4);
            me.dayWeek=me.date.getDay();
            me.id = me.year  + me.month + me.day;
        }
    } catch (e) { alert('_giorni.init ERROR: ' + e.message)}
    
};

/* _pratica */
var _pratica = function (dati){
    
    var me = this;
    
    me.idVeicolo = '';
    me.autoSostitiva = false;
    me.idAuto = '';
    me.stato = '';
    me.descrizione = '';
    me.targa = '';
    me.dataIn = '';
    me.oraIn = '';
    me.dataOut = '';
    me.oraOut = '';
    me.idColore = '';
    
    me.html = null;

    me.render = function(g){
        me.html = '<tr class="pratiche" data-stato="' + me.stato + '">' + 
                '<td class="selector">&nbsp;</td>' +
                '<td class="stato">' + me.stato + '</td>' + 
                '<td class="descrizione">' + me.descrizione + '</td>' + 
                '<td class="targa">' + me.targa + '</td>' + 
                '<td>' + (me.autoSostitiva ? '<center>SI</center>' : '' ) + '</td>' +
                '<td>' + (me.autoSostitiva ? ottieniAutoSostitutiva(me.idAuto)  :'') + '</td>' + 
                '</giorni>' + 
                '</tr>';
        
    };
    
    me.colore = function(){
      var c = calendario.colori.item(me.idColore);
      return c.colore;
    }
     
    try {
        if (dati) {
            // Uso l'extend perchè il dati sarà definito come un oggetto di questo tipo
            $.extend(me, dati);
          
        }
    } catch (e) { alert('_pratica.init ERROR: ' + e.message)}
    
}

var _oggi = function(data){
    var me = this;
    me.date;
    me.day=0;
    me.month=0;
    me.year=0;
   // me.dayWeek=0;
    
    me.getDate = function(){
        return me.date;
    }
  
    me.dayInMonth = function(){
        return  me.date.getMonthDayCount();
    }
    
    me.dayInMonthPrec = function(){
        var date = me.date;
        date.setMonth(date.getMonth() - 1);
        return  date.getMonthDayCount();
    }
    
    me.dayWeekName = function(){
        return dayName[me.dayWeek()];
    }
    
    me.dayWeekNameLower = function(){
        return dayName[me.dayWeek()].toLowerCase();
    }
    
    me.dataEstesa = function(){
        return ottieniDataFormattata(me.day, me.month, me.year)
    }
    
    me.dayWeek = function(){
        return me.date.getDay();
    }

    if (data){
        me.date = data;
        me.month = data.getMonth()+1;
        me.day = data.getDate();
        me.year = data.getFullYear();
    }
}

/* load */
var calendario;
var oggi;

$(document).ready(function() {

    $('#cmbStato').on('change', function(){calendario.applicaFiltri()});

    /* Inizializzo oggi come giorno per definire il periodo di riferimento per la visualizzazione */ 
    /* ------------------------------------------------------------------------------------------ */
    oggi = new _oggi(new Date());
    /* ------------------------------------------------------------------------------------------ */
    
    /* Inizializzo l'oggetto calendario che gestisce tutta l'interfaccia */
    /* ----------------------------------------------------------------- */
    calendario = new _calendario();
    calendario.render();
    /* ----------------------------------------------------------------- */

    /* Comportamento click su bottoni periodo */
    /* -------------------------------------- */
    $('.periodo').click(function(){
        $('.periodo').each(function(){$(this).removeClass('selected').addClass('not-selected');})
        $(this).removeClass('not-selected').addClass('selected');
        calendario.vediCome = $(this).data('vedicome');
        calendario.render();
    });
    /* -------------------------------------- */
    
    /* Comportamento click su bottoni indietro e avanti */
    /* ------------------------------------------------ */
    $('.sub').click(function(){
        if (calendario.spostamento){
            oggi.date.setDate(oggi.date.getDate() - calendario.spostamento);
        } else {
            oggi.date.setMonth(oggi.date.getMonth() - 1);
        }
        oggi = new _oggi(oggi.date);
        calendario.render();
    });
    
    $('.add').click(function(){
        if (calendario.spostamento){
            oggi.date.setDate(oggi.date.getDate() + calendario.spostamento);
        } else {
            oggi.date.setMonth(oggi.date.getMonth() + 1);
        }
        oggi = new _oggi(oggi.date);
        calendario.render();
    });
    /* ------------------------------------------------ */

});

/* FUNZIONI */ 
function ottieniLavorazione(id){
    var classe ='cella';
    $.each(lavorazioni, function (l) {
        var lav = lavorazioni[l];
        if (lav.id == id){
            classe = lav.descrizione;    
        }
    });
    return classe;
}

function ottieniAutoSostitutiva(idAuto){
    var autoS ='';
    $.each(auto, function (a) {
        var au = auto[a];
        if (au.idauto == idAuto){
            autoS = au.descrizione + ' ' + au.targa;    
        }
    });
   
    return autoS;
}

function ottieniDataFormattata(day, month, year){
  return (day < 10 ? '0' + day : day) + '/' + 
         (month < 10 ? '0' + month : month) + '/' + 
          year ;
}


function ottieniGiornoData(day, month, year){
    return new _oggi (new Date(year, month - 1, day));
}


function calcolaGiorniPeriodo(periodo){
    
    if (periodo){
        var ddDiff = 0;
             
        switch (dayName[oggi.dayWeek] ) {
           case dayName.Domenica:
               ddDiff = 6 * -1;
               break;
           default:
               ddDiff = (oggi.dayWeek - 1)  * -1;
               break;
        }; 
        
        var day  = oggi.day;
        var month  = oggi.month;
        var year  = oggi.year;
    
        for (var i=1; i <= periodo; i++){
           var _data = new Date(year + '-' + month + '-' + day);
           _data.setDate(_data.getDate() + ddDiff);
         
           var _g = new _oggi(_data);
           var data = ottieniDataFormattata(_g.day, _g.month, _g.year);
           var nome = ottieniGiornoData(_g.day, _g.month, _g.year).dayWeekName();
           calendario.giorni.add('td_gg_' + i, new _giorno({ data: data, nome: nome }));
           
           ddDiff++;
        }
    
    } else{
       /* Devo calcolare i giorni del mese precedente che disegno */
       ottieniGiorniMesePrecedente(new Date(oggi.year + '-' + oggi.month + '-' + 1)) 
        
       /* Questi sono i giorni esatti del mese */
       for (i = 1; i <= oggi.dayInMonth(); i++) { 
          var data = ottieniDataFormattata(i, oggi.month, oggi.year);
          var nome = ottieniGiornoData(i, oggi.month, oggi.year).dayWeekName();
          calendario.giorni.add('td_gg_' + i, new _giorno({ data: data, nome: nome }));
       }
       
       /* Devo calcolare i giorni del mese successivo che disegno */
       ottieniGiorniMeseSuccessivo(new Date(oggi.year + '-' + oggi.month + '-' + oggi.dayInMonth())) 
    }
}

function ottieniStrutturaSettimana(g, data){
    var ret = '';
    var diff = (g);
    
    try {
      var month = data.getMonth()+1;
      var day = data.getDate();
      var year = data.getFullYear();
      
      for (var i = 1; i < g; i++){
          var _data = new Date(year + '-' + month + '-' + day);
          _data.setDate(_data.getDate() - (diff - i));
          var _g = new _oggi(_data);
          
          var data = ottieniDataFormattata(_g.day, _g.month, _g.year);
          var nome = ottieniGiornoData(_g.day, _g.month, _g.year).dayWeekName();
          var _gg = new _giorno({ data: data, nome: nome });
  
          ret += '<td id="day_' + _gg.id + '">' + 
                 ' <div class="cellaMesePrec" title="' + _gg.data + '">' + 
                 '  <table>' +
                 '     <tr><td class="int">'  + parseInt(_gg.day) + (parseInt(_gg.day) == _gg.dayInMonth() ? ' ' + monthShortName[parseInt(_gg.month) -1].toLowerCase() :'') + '<td></tr>' +
                 '     <tr id="cont_' + _gg.id + '"><td class="cont"><td></tr>' +
                 '  </table>' +
                 ' </div>' + 
                 '</td>';
               
      }
    } catch (e) { alert('ottieniStrutturaSettimana() ERROR: ' + e.message)};
    
    return ret;
}  

/* Giorni mese precedente e successivo */
/* ----------------------------------- */
// se sto disegnando la prima settimana o l'ultima devo calcolare i giorni "vuoti" prima del primo giorno del mese
// tutte le altre settimane sono di 7 gg eventualmente alcune celle saranno da svuotare dopo
/* ----------------------------------- */
// 0:dom, 1: lun, 2:mar, 3:mer, 4:gio, 5:ven, 6:sab
function ottieniGiorniMesePrecedente(dataInizioMese){
    var ret = '';
    var g = dataInizioMese.getDay();
    var diff = (g);
    //alert('oggi dayweek:' + g);
    
    var month = dataInizioMese.getMonth()+1;
    var day = dataInizioMese.getDate();
    var year = dataInizioMese.getFullYear();
    
    //alert(day + '-' + month + '-' + year + ' : ' + dayName[g])
    
    // se il primo giorno del mese è domenica creo anche tutti i giorni che precedono la data corrente
    if (g == 0){
         for (i = 5; i > 0; i--){
            var _data = new Date(year + '-' + month + '-' + day);
            _data.setDate(_data.getDate() - i);
            var _g = new _oggi(_data);
            
            var data = ottieniDataFormattata(_g.day, _g.month, _g.year);
            var nome = ottieniGiornoData(_g.day, _g.month, _g.year).dayWeekName();
            calendario.giorni.add('td_gg_' + i * -1, new _giorno({ data: data, nome: nome }));
                 
        }
    }else {
        for (var i = 1; i < g; i++){
            var _data = new Date(year + '-' + month + '-' + day);
            _data.setDate(_data.getDate() - (diff - i));
            var _g = new _oggi(_data);
            
            var data = ottieniDataFormattata(_g.day, _g.month, _g.year);
            var nome = ottieniGiornoData(_g.day, _g.month, _g.year).dayWeekName();
            calendario.giorni.add('td_gg_' + (diff - i) * -1, new _giorno({ data: data, nome: nome }));
                 
        }
    }
}

function ottieniGiorniMeseSuccessivo(dataFineMese){
    var ret = '';
    var g = dataFineMese.getDay();
    
    try {
       var month = dataFineMese.getMonth()+1;
       var day = dataFineMese.getDate();
       var year = dataFineMese.getFullYear();
     
       // partendo dal giorno della settimana dell'ultimo giorno del mese corrente devo riempire la settimana che sfora nel prossimo mese
       for (var i = 1; i <= (7 - g); i++){
           var _data = new Date(year + '-' + month + '-' + day);
           _data.setDate(_data.getDate() + i);
           var _g = new _oggi(_data);
           var data = ottieniDataFormattata(_g.day, _g.month, _g.year);
           var nome = ottieniGiornoData(_g.day, _g.month, _g.year).dayWeekName();
           calendario.giorni.add('td_gg_' + (i + 100), new _giorno({ data: data, nome: nome }));
       }
    } catch (e) { alert('ottieniGiorniMeseSuccessivo() ERROR: ' + e.message)};
}
