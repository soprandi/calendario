// COLLECTION
//----------------------------------------------------------------------------------------------------------------------------------------
var Collection = function () {
    this.count = 0;
    this.collection = {};
    this.k = []
    this.add = function (key, item) {
        if (this.collection[key] != undefined)
            return undefined;
        this.collection[key] = item;
        this.k[this.k.length] = key;
        return ++this.count
    }
    this.remove = function (key) {
        if (this.collection[key] == undefined)
            return undefined;
        delete this.collection[key]
        return --this.count
    }
    this.item = function (key) {
        return this.collection[key];
    }
    this.forEach = function (block) {
        for (key in this.collection) {
            if (this.collection.hasOwnProperty(key)) {
                block(this.collection[key], key);
            }
        }
    }
    this.forEachNoSort = function (block) {
        for (var i = 0; i < this.k.length; i++) {
            var key = this.k[i];
            if (this.collection.hasOwnProperty(key)) {
                block(this.collection[key], key);
            }
        }
    }

}

// DATE CLASS EXTENSION
//----------------------------------------------------------------------------------------------------------------------------------------
// Provide month names
var monthName = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];
var monthShortName = ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'];
var dayName = ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'];
var dayShortName = ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'];

function getMonthName(m) {
    if (m && m > 0 && m < 13) {
        return monthName[m - 1]
    }
    return '';
}
function getMonthShortName(m) {
    if (m && m > 0 && m < 13) {
        return monthShortName[m - 1]
    }
    return '';
}
function getDayName(d) {
    if (d >= 0 && d < 7) {
        return dayName[d]
    }
    return '';
}
function getDayShortName(d) {
    if (d >= 0 && d < 7) {
        return dayShortName[d]
    }
    return '';
}

Date.prototype.getMonthName = function () {
    return monthName[this.getMonth()];
}

// Provide month abbreviation
Date.prototype.getMonthAbbr = function () {
    return monthShortName[this.getMonth()];
}

// Provide full day of week name
Date.prototype.getDayFull = function () {
    return dayName[this.getDay()];
};

// Provide abbr day of week name
Date.prototype.getDayAbbr = function () {
    return dayShortName[this.getDay()];
};

// Provide the day of year 1-365
Date.prototype.getDayOfYear = function () {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((this - onejan) / 86400000);
};

// Provide the day suffix (st,nd,rd,th)
Date.prototype.getDaySuffix = function () {
    var d = this.getDate();
    var sfx = ["th", "st", "nd", "rd"];
    var val = d % 100;

    return (sfx[(val - 20) % 10] || sfx[val] || sfx[0]);
};

// Provide Week of Year
Date.prototype.getWeekOfYear = function () {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
}

// Provide if it is a leap year or not
Date.prototype.isLeapYear = function () {
    var yr = this.getFullYear();

    if ((parseInt(yr) % 4) == 0) {
        if (parseInt(yr) % 100 == 0) {
            if (parseInt(yr) % 400 != 0) {
                return false;
            }
            if (parseInt(yr) % 400 == 0) {
                return true;
            }
        }
        if (parseInt(yr) % 100 != 0) {
            return true;
        }
    }
    if ((parseInt(yr) % 4) != 0) {
        return false;
    }
};

// Provide Number of Days in a given month
Date.prototype.getMonthDayCount = function () {
    var month_day_counts = [
                                    31,
                                    this.isLeapYear() ? 29 : 28,
                                    31,
                                    30,
                                    31,
                                    30,
                                    31,
                                    31,
                                    30,
                                    31,
                                    30,
                                    31
                                ];

    return month_day_counts[this.getMonth()];
}

// format provided date into this.format format
Date.prototype.format = function (dateFormat) {
    // break apart format string into array of characters
    dateFormat = dateFormat.split("");

    var date = this.getDate(),
            month = this.getMonth(),
            hours = this.getHours(),
            minutes = this.getMinutes(),
            seconds = this.getSeconds();
    // get all date properties ( based on PHP date object functionality )
    var date_props = {
        d: date < 10 ? '0' + date : date,
        D: this.getDayAbbr(),
        //j: this.getDate(),
        j: date < 10 ? '0' + date : date,
        l: this.getDayFull(),
        S: this.getDaySuffix(),
        w: this.getDay(),
        z: this.getDayOfYear(),
        W: this.getWeekOfYear(),
        F: this.getMonthName(),
        m: month < 9 ? '0' + (month + 1) : month + 1,
        M: this.getMonthAbbr(),
        n: month + 1,
        t: this.getMonthDayCount(),
        L: this.isLeapYear() ? '1' : '0',
        Y: this.getFullYear(),
        y: this.getFullYear() + ''.substring(2, 4),
        a: hours > 12 ? 'pm' : 'am',
        A: hours > 12 ? 'PM' : 'AM',
        g: hours % 12 > 0 ? hours % 12 : 12,
        G: hours > 0 ? hours : "12",
        h: hours % 12 > 0 ? hours % 12 : 12,
        H: hours,
        i: minutes < 10 ? '0' + minutes : minutes,
        s: seconds < 10 ? '0' + seconds : seconds
    };

    // loop through format array of characters and add matching data else add the format character (:,/, etc.)
    var date_string = "";
    for (var i = 0; i < dateFormat.length; i++) {
        var f = dateFormat[i];
        if (f.match(/[a-zA-Z]/g)) {
            date_string += date_props[f] ? date_props[f] : '';
        } else {
            date_string += f;
        }
    }

    return date_string;
};

// FUNCTION
//----------------------------------------------------------------------------------------------------------------------------------------
function strVal(val) {
    return val ? val.replace(new RegExp('\ç', 'g'), String.fromCharCode(10)).replace(new RegExp('\&#231;', 'g'), String.fromCharCode(10)).replace(new RegExp('\§', 'g'), '"') : '';
}

function strValToServer(val) {
    return val ? '"' + val.toString().replace(new RegExp(String.fromCharCode(10), 'g'), 'ç').replace(new RegExp(String.fromCharCode(10), 'g'), '&#231;').replace(new RegExp('\"', 'g'), '§') + '"' : ''
}

function strValToUrl(val) {
    return val ? val.replace(new RegExp(String.fromCharCode(10), 'g'), 'ç').replace(new RegExp(String.fromCharCode(10), 'g'), '&#231;').replace(new RegExp('\"', 'g'), '§') : ''
}

function bitVal(v) {
    if (v == "0") {
        return 0
    }
    return v ? v : "";
}

function parseDate(input) {
    if (input) {
        var parts = input.match(/(\d+)/g);
        return new Date(parts[2], parts[1] - 1, parts[0], parts[3], parts[4], parts[5]);
    }
    return "";
}

function setVal(nome, valore, tipo) {
    if (nome) { scheda[nome] = valore; }

    if (valore || tipo == 3) {
        if (tipo == 1) {
            return strValToServer(valore);
        } else if (tipo == 2) {
            return strValToServer(valore.format("j/m/Y h:i:s"));
        } else if (tipo == 4) {
            return parseFloat(valore.replace(",", ".").replace("€", ""));
        } else {
            return valore;
        }
    } else {
        return "~";
    }
}

function dateVal(val) {
    return val ? new Date(val) : "";
}

function moneyFormat(i) {
    //return (i && !isNaN(i)) ? i.toFixed(2).toLocaleString('it-IT') : '';
    return (i && !isNaN(i)) ? i.toLocaleString('it-IT' , {
                                                style: 'currency', 
                                                currency: 'EUR', 
                                                minimumFractionDigits: 2 
                                            }) : '';
}
function moneyVal(v) {
    return v ? parseFloat(v.replace(/[.]/g, '').replace(',', '.')) : 0;
}

function numberFormat(n) {
    return (n && !isNaN(n)) ? n.toLocaleString('it-IT').replace(',00', '') : 0;
}
function intNumVal(val, valIfNaN) {
    if (val && !isNaN(val)) {
        return parseInt(val);
    }
    return (valIfNaN || valIfNaN == 0) ? valIfNaN : '';
}

function numVal(val, valIfNaN) {
    if (val && !isNaN(val)) {
        return parseFloat(val);
    }
    return (valIfNaN || valIfNaN == 0) ? valIfNaN : '';
}

function intNumMask(e, obj) {
    var chr = e.charCode ? e.charCode : e.keyCode;

    // Allow: backspace, delete, tab, escape, and enter
    if (chr == 46 || chr == 8 || chr == 9 || chr == 27 ||
    // Allow: home, end, left, right
        chr == 35 || chr == 36 || chr == 37 || chr == 39) {
        // let it happen, don't do anything
        return;
    }
    else if (chr == 13) {
        return;
    }
    // Up (incrementa valore di 1)
    else if (chr == 38) {
        e.returnValue = false;
        e.cancel = true;
        e.preventDefault();
        var o = $(obj);
        if (o.hasClass("intNum")) {
            var v = intNumVal(o.val());
            var m = o.attr("max") ? intNumVal(o.attr("max")) : "";
            if ((m && v >= m)) { return; }
            o.val(v + 1);
        }
    }
    // Down (decrementa valore di 1)
    else if (chr == 40) {
        e.returnValue = false;
        e.cancel = true;
        e.preventDefault();
        var o = $(obj);
        if (o.hasClass("intNum")) {
            var v = intNumVal(o.val());
            if (v > 0) {
                o.val(v - 1);
            }
        }
    }
    else {
        if (e.shiftKey || (chr < 48 || chr > 57) && (chr < 96 || chr > 105)) {
            e.preventDefault();
        }
    }
}

function numMask(e, obj) {
    var chr = e.charCode ? e.charCode : e.keyCode;

    // Allow: backspace, delete, tab, escape, and enter
    if (chr == 46 || chr == 8 || chr == 9 || chr == 27 ||
    // Allow: home, end, left, right
        chr == 35 || chr == 36 || chr == 37 || chr == 39) {
        // let it happen, don't do anything
        return;
    } else if (chr == 13) {
        return;
    // Virgola
    } else if (chr == 188 || chr == 110 || chr == 190) {
        e.returnValue = false;
        e.cancel = true;
        e.preventDefault();
        var o = $(obj),
            v = o.val();
        if (v.indexOf(',') < 0) {
            if (v.length == 0) {
                o.val(v + '0,');
            } else {
                o.val(v + ',');
            }
        }
    // Meno
    } else if (chr == 189 || chr == 109) {
        e.returnValue = false;
        e.cancel = true;
        e.preventDefault();
        var o = $(obj),
            v = o.val();
        if (v.indexOf('-') < 0) {
            o.val('-' + v);
        }
    } else if (e.shiftKey || (chr < 48 || chr > 57) && (chr < 96 || chr > 105)) {
        e.preventDefault();
    }
}

function getErrMsg(msg) {
    return '<tr height="5"><td colspan="2"></td></tr><tr valign="center"><td><img src="img/Errore.gif" alt="errore non bloccante"></td><td class="Etichetta">' + msg + '</td></tr>';
}

// COOKIE
//----------------------------------------------------------------------------------------------------------------------------------------

function cookieSet(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}

function cookieGet(name) {
    var nameEQ = escape(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function cookieDel(name) {
    cookieSet(name, "", -1);
}